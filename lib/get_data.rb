require_relative "data_sync"

class Get_data
  def news
    news_base = "https://newsapi.org/v1/articles?source="
    news_query = "&apikey=3e22f2fcc1344975ae2b2e69379e2a6e"
    news_types = [
      'recode', 'techcrunch', 'newsweek', 'national-geographic',
      'bbc-news', 'espn', 'hacker-news', 'the-huffington-post',
      'the-verge'
      ]
    news_dir = File.expand_path('../../_data/feeds/news', __FILE__)

    puts "getting news data files"
    news_types.each do |news|
      file_path = "#{news_dir}/#{news}.json"
      uri = news_base + news + news_query
      ds = Data_sync.new(uri, file_path)
      ds.save
    end
  end
end

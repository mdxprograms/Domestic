require "net/http"
require "json"

class Data_sync
  def initialize(url, file_path)
    @url = url
    @file_path = file_path
  end

  def save
    url = URI(@url)
    res = Net::HTTP.get(url)
    data = JSON.parse(res)
    json_dump = JSON.pretty_generate(data)
    
    File.delete(@file_path) if File.file?(@file_path)
    File.open(@file_path, 'w+') { |file| file.write(json_dump) }
  end
end

'use strict';
var request = require('request');

module.exports.syncSources = (event, context, callback) => {
  var url = "https://api.netlify.com/build_hooks/5879a28c71e20a7a00deeff7";

  request.post(url, {}, function (error) {
    if (!error) {
      const response = {
        statusCode: 200,
        body: JSON.stringify({
          message: 'Domestic sync ran',
        }),
      };

      callback(null, response);
    }
  });
};

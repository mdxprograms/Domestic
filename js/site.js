(function () {
  'use strict';
  var toggle = document.querySelector('nav .toggle'),
    links = document.querySelector('nav .links'),
    gridItems = document.querySelectorAll('.grid li'),
    matchList = document.getElementById('matches'),
    search = document.querySelector('.search'),
    searchQuery = search.querySelector('input'),
    closeSearch = search.querySelector('#close'),
    titleCollection = [];

  gridItems.forEach(function(item) {
    titleCollection.push({
      id: item.querySelector('a').dataset.id,
      title: item.dataset.title.toLowerCase(),
      source: item.parentNode.parentNode.id.toLowerCase()
    });
  });

  toggle.addEventListener('click', function (e) {
    e.preventDefault();

    links.classList.toggle('active');
  });

  links.addEventListener('click', function () {
    this.classList.remove('active');
  });


  document.addEventListener('keydown', function(e) {
    if (e.which === 81) {
      e.preventDefault();

      search.classList.add('show');
      searchQuery.focus();
      activeClose();
      watchSearch();
    }
  });

  function activeClose() {
    closeSearch.addEventListener('click', function() {
      titleCollection = [];
      search.classList.remove('show');
    });
  }

  function watchSearch() {
    searchQuery.addEventListener('keyup', getResults);
  }

  function getResults() {
    matchList.innerHTML = '';
    if (searchQuery.value.length > 1) {
      var match = titleCollection.filter(function(item) {
        return (item.title.includes(
            searchQuery.value.toLowerCase()) ||
            item.source.includes(searchQuery.value.toLowerCase())
        );
      });

      if (match.length > 0) {
        match.forEach(function(m) {
          matchList.innerHTML += '<a href="" data-target-id="' + m.id + '">' + m.source + ' - ' + m.title + '</a>';
        });

        watchLinkClick();
      } else {
        matchList.innerHtml = '';
      }
    } else {
      matchList.innerHtml = '';
    }
  }

  function watchLinkClick() {
    matchList.querySelectorAll('a').forEach(function(link) {
      link.addEventListener('click', function(e) {
        e.preventDefault();
        document.querySelector('[data-id="' + link.dataset.targetId + '"]')
          .parentNode.scrollIntoView();
        search.classList.remove('show');
      })
    })
  }

}());
